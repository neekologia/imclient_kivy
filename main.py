import kivy
kivy.require('2.1.0') # replace with your current kivy version !

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.properties import Property
from kivy.clock import Clock
import socket

message_id = 0
user = "anon"
room = "entryway"
bg_c = "#121212"
box_c = "#090a0a"
btn_c = "#284165"

class JoinButton(Button):
	def set_theme(self):
		self.background_color = btn_c
		self.background_normal = ""

class UserInfo(TextInput):
	def set_theme(self):
		self.cursor_color = "#FFFFFF"
		self.background_color = box_c
		self.background_normal = ""
		self.foreground_color = "#FFFFFF"
		self.padding = self.minimum_height * 1.2

class Chat(Label):
	def on_size(self, *args):
		self.padding = (10.0, 10.0)
		self.text_size[0] = self.width

class ChatInput(TextInput):
	def set_theme(self):
		self.cursor_color = "#FFFFFF"
		self.background_color = box_c
		self.background_normal = ""
		self.foreground_color = "#FFFFFF"
		self.padding = self.height / 6

	def on_focus(self, *args):
		App.get_running_app().root.update_scrollview()

	def s_write(self):
		global message_id
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect(('archneek.zapto.org', 14114))
		reply = "\n".join(["input", "".join([user, ": ", self.text]),
			"", room]).encode('utf-8')
		s.sendall(bytes(reply))
		data = s.recv(64000).decode('utf-8')
		s.close()
		message_id = data.splitlines()[0]
		messages = "\n".join(data.splitlines()[1:])
		App.get_running_app().root.update_chat(messages)

	def keyboard_on_key_down(self, window, keycode, text, modifiers):
		if keycode[1] == "enter":
			if len(modifiers) == 0:
				self.s_write()
				self.text = ""
			elif modifiers[0] == "shift":
				self.text += "\n"
		elif keycode[1] == "backspace":
			xpos = self.cursor[0]
			self.text = self.text[:xpos - 1] + self.text[xpos:]
			self.cursor = (xpos - 1, self.cursor[1])
		elif keycode[1] == "right":
			self.cursor = ((self.cursor[0] + 1), self.cursor[1])
		elif keycode[1] == "left":
			self.cursor = ((self.cursor[0] - 1), self.cursor[1])

class Screen(BoxLayout):
	def __init__(self, **kwargs):
		super(Screen, self).__init__(**kwargs)
		Window.clearcolor = bg_c
		self.login()

	def update_chat(self, chat_input):
		self.chat_output.text += '\n' + chat_input
		self.update_scrollview()

	def update_scrollview(self):
		self.chat_output.texture_update()
		self.chat_output.size[1] = self.chat_output.texture_size[1]
		if self.scroll_view.scroll_y == 1:
			self.scroll_view.scroll_y = 0

	def start_chat(self):
		self.orientation = 'vertical'
		self.padding = Window.height / 25
		self.spacing = Window.height / 25
		
		self.chat_output = Chat(
			size_hint_y = None,
			text = "",
			font_size = "20sp",
			halign = "left",
			valign = "top"
		)
		
		self.chat_input = ChatInput(
			multiline=True,
			size_hint_y=None,
			height=(Window.height/10)
		)
		self.chat_input.set_theme()

		self.scroll_view = ScrollView()
		self.scroll_view.add_widget(self.chat_output)
		self.add_widget(self.scroll_view)
		self.add_widget(self.chat_input)

		Window.softinput_mode = 'pan'
		Clock.schedule_interval(self.s_updates, 0.5)

	def join(self, *args):
		self.clear_widgets()
		self.start_chat()

	def login(self):
		def on_text_user(set_user, value):
			global user
			user = value
		def on_text_room(set_room, value):
			global room
			room = value
		
		self.orientation = 'vertical'
		pad = Window.height / 10
		self.padding = pad
		self.spacing = pad / 2
		
		self.user_label = Label(
			font_size = "24sp",
			text = "username",
			size_hint_y = None
		)
		self.user_label.size = self.user_label.texture_size
		self.room_label = Label(
			font_size = "24sp",
			text = "room",
			size_hint_y = None
		)
		self.room_label.size = self.room_label.texture_size

		self.set_room = UserInfo(
			multiline = False,
			halign = 'center',
			font_size = "20sp",
			size_hint_y = None
		)
		self.set_room.set_theme()
		self.set_room.bind(text=on_text_room)

		self.set_user = UserInfo(
			multiline = False,
			halign = 'center',
			font_size = "20sp",
			size_hint_y = None,
		)
		self.set_user.set_theme()
		self.set_user.bind(text=on_text_user)
		
		self.join_button = JoinButton(
			text="join",
			size_hint_y = None
		)
		self.join_button.set_theme()
		self.join_button.bind(on_press=self.join)
		
		self.add_widget(self.room_label)
		self.add_widget(self.set_room)
		self.add_widget(self.user_label)
		self.add_widget(self.set_user)
		self.add_widget(self.join_button)

	def s_updates(self, delta_time):
		global message_id
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect(('archneek.zapto.org', 14114))
		reply = "\n".join(["update", str(message_id), room]).encode('utf-8')
		s.sendall(bytes(reply))
		data = s.recv(64000).decode('utf-8')
		s.close()
		if data != "":
			message_id = data.splitlines()[0]
			messages = "\n".join(data.splitlines()[1:])
			self.update_chat(messages)

class MyApp(App):

	def build(self):
		return Screen()

if __name__ == '__main__':
	MyApp().run()
